x, y, z = "Orange", "Banana", "Cherry"
print(x)
print(y)
print(z)
# this is actually useful
a = b = c = "Orange"
print(a)
print(b)
print(c)
fruits = ["apple", "banana", "cherry"]
d, e, f = fruits
print(d)
print(e)
print(f)
