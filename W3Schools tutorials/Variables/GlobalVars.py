x = "a mess"

def myfunc():
    global y
    y = "fantastic"
    global x
    x = "really is a mess"
    print("Python is " + y)

myfunc()
print("Python is " + x)
print(y)